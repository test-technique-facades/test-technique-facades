package com.orange.facade.testtechnique.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

@lombok.Builder
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class CreateCustomerException extends Exception {

	@JsonProperty("errorTitle")
	private String errorTitle;

	@JsonProperty("errorDescription")
	private String errorDescription;

	public CreateCustomerException errorTitle(String errorTitle) {
		this.errorTitle = errorTitle;
		return this;
	}

	/**
	 * Get errorTitle
	 *
	 * @return errorTitle
	 */

	@Schema(name = "errorTitle", example = "GENERIC ERROR", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
	public String getErrorTitle() {
		return errorTitle;
	}

	public void setErrorTitle(String errorTitle) {
		this.errorTitle = errorTitle;
	}

	public CreateCustomerException errorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
		return this;
	}

	/**
	 * Get errorDescription
	 *
	 * @return errorDescription
	 */

	@Schema(name = "errorDescription", example = "THIS IS A GENERIC ERROR", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CreateCustomerException errorResponse = (CreateCustomerException) o;
		return Objects.equals(this.errorTitle, errorTitle) &&
				Objects.equals(this.errorDescription, errorDescription);
	}

	@Override
	public int hashCode() {
		return Objects.hash(errorTitle, errorDescription);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CreateCustomerException {\n");
		sb.append("    errorTitle: ").append(toIndentedString(errorTitle)).append("\n");
		sb.append("    errorDescription: ").append(toIndentedString(errorDescription)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
