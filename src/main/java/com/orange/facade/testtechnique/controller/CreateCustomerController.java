package com.orange.facade.testtechnique.controller;

import com.orange.facade.model.CustomerApiResponse;
import com.orange.facade.model.CustomerDTO;
import com.orange.facade.model.ErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@RequestMapping(path = "/")
public class CreateCustomerController {

	@Operation(
			summary = "Create ProductOrder",
			method = "POST",
			description = "This operation creates a productOrders.",
			responses = { @io.swagger.v3.oas.annotations.responses.ApiResponse(
					responseCode = "201",
					description = "Created",
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(
									implementation = CustomerApiResponse.class
							)
					) }
			), @io.swagger.v3.oas.annotations.responses.ApiResponse(
					responseCode = "400",
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(
									implementation = ErrorResponse.class
							)
					) }
			), @ApiResponse(
					responseCode = "500",
					content = { @Content(
							mediaType = "application/json",
							schema = @Schema(
									implementation = Exception.class
							)
					) }
			) }
	)
	@PostMapping(
			value = { "/createCustomer" },
			produces = { "application/json; charset=utf-8" },
			consumes = { "application/json; charset=utf-8" }
	)
	@ResponseBody
	public ResponseEntity<?> createCustomerPost(@RequestBody(required = true) @Valid CustomerDTO customerDTO) {
		return new ResponseEntity<>(new CustomerApiResponse(), HttpStatus.CREATED);
	}
}
