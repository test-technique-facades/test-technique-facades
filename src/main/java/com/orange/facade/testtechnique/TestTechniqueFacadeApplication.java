package com.orange.facade.testtechnique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTechniqueFacadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestTechniqueFacadeApplication.class, args);
	}

}
