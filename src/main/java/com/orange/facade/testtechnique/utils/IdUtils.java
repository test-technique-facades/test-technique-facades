package com.orange.facade.testtechnique.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class IdUtils {

	public static String generateIdInternet() {
		return RandomStringUtils.randomAlphabetic(10);
	}

	public static Long generateIdMobile() {
		return Math.abs(new Random().nextLong());
	}
}
