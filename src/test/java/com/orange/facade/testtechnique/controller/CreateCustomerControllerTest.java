package com.orange.facade.testtechnique.controller;

import com.orange.facade.model.CustomerDTO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class CreateCustomerControllerTest {

	@InjectMocks
	CreateCustomerController createCustomerController;

	@Test
	void createCustomerPostTestOk() throws Exception {
		ResponseEntity<?> responseEntity = createCustomerController.createCustomerPost(new CustomerDTO());

		Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(201));
	}
}
