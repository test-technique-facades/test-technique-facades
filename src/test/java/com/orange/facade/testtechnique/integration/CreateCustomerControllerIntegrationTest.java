package com.orange.facade.testtechnique.integration;

import com.orange.facade.testtechnique.TestTechniqueFacadeApplication;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestTechniqueFacadeApplication.class)
@WebAppConfiguration
@ActiveProfiles("test")
class CreateCustomerControllerIntegrationTest {

	protected MockMvc mvc;
	@Autowired
	WebApplicationContext webApplicationContext;

	@BeforeAll
	protected void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	void createCustomerControllerIntegrationOk() throws Exception {
		//Setup
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/json");

		mvc.perform(
						//Test
						post("/createCustomer").headers(httpHeaders).content("""
								{
								  "idCustomer": 1234,
								  "firstname": "Test",
								  "lastname": "FACADES",
								  "address": "42 avenue artistide briand 92220 Bagneux",
								  "birthDate": "2023-03-21",
								  "birthCity": "Bagneux",
								  "birthDepartment": 92,
								  "emailAddress": "test.technique.facade@orange.com",
								  "phoneNumber": "0600000000",
								  "contractType": "MOBILE"
								}
								"""))
				.andDo(print())

				//Assert
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType("application/json;charset=utf-8"))
				.andExpect(jsonPath("$").exists());
	}
}

