# Test Technique Façade

## Prérequis

- Java 21 minimum (Spring boot 3.4.3)
- Maven

## Contexte

Dans le cadre du développement d'une API de création de client, vous devez réaliser une API en respectant les contraintes fonctionnelles.

## Durée

Le test doit durer environ 2h, l'objectif n'est pas forcément de le terminer mais de donner de quoi discuter pendant l'entretien

## Éléments fournis

Un contrôleur vous est fourni.

Une classe d'exception est également fournie.

Un test d'intégration Ok est fourni.

Le swagger de l'application est fourni (dans resources/swagger/testSwagger.json)

Une classe IdUtils qui permet de générer les id internet et mobile.

## Objectifs

Réaliser une API qui prend un objet CustomerDTO en entrée et renvoie un CustomerApiResponse si tout se passe bien, sinon renvoi un ErrorResponse ou une exception.

## Règles fonctionnelles

- Vérification de l’intégrité de tous les champs

- firstname est strictement inférieur à 15 caractères
- lastname est en majuscules
- birthDate, la personne à plus de 18 ans
- birthDepartement est strictement inférieur à 1000
- emailAddress, vérification du format mail
- phoneNumber est égal à 10 caractères, uniquement des chiffres commencent par un zéro

- Si contractType == INTERNET, on génère un id internet et laisse l’autre à null
- Si contractType == MOBILE, on génère un id mobile et laisse l’autre à null
- Si contractType == OPEN, on génère un id internet et un id mobile

- Une offre contient forcément une option (internet ou mobile)
- Une offre contient maximum une option internet et une option mobile
- Il ne peut pas y avoir plus de deux discounts par option


Si toutes les règles sont ok, on répond **200** avec un **CustomerApiResponse**. La date d'inscription à ajouter est la date du jour.

Si une ou plusieurs règles sont **KO**, on renvoie une erreur **400** via l'API avec un **CreateCustomerException** qui contient :

| ErrorTitle | Error Description                |
| ---------- |----------------------------------|
 | Error when calling create Customer endpoint | Error on field : {{ fieldName }} |

FieldName correspondant au nom du premier champ en erreur dans l'ordre des règles fonctionnelles ci-dessus (par exemple si aucune règle fonctionnelle ne fonctionne, on renverra firstName qui est la première règle fonctionnelle).

Si l'option **FREE** est présent dans les offres.options.discounts, on doit renvoyer une erreur **500** avec une **exception** (vous avez le choix sur le type d'exception).

## Règles techniques

Vous pouvez rajouter des librairies ou modifier le pom à votre guise, le projet n'est pas bloqué à ce niveau.

Vous n'avez pas l'autorisation de modifier le swagger et vous devez obligatoirement utiliser les classes générées par le swagger.

## Aide

Pour toute demander d'aide, vous pouvez contacter Yanis Guérault [(yanis.guerault@orange.com)](yanis.guerault@orange.com), Johan Manier [(johan.manier@orange.com)](johan.manier@orange.com) ou Gaetan Pierre [(gaetan.pierre@orange.com)](gaetan.pierre@orange.com)

Les classes du swagger (CustomerApiResponse, CustomerDTO et ErrorResponse) sont déjà générées par le projet. Il suffit de faire un ```mvnwd compile``` dans le projet pour les générer dans le dossier target

## Rendu

Pour le rendu, il faudra forker le projet sur [gitlab.com](https://gitlab.com) et nous présenter le lors de l'entretien ainsi que nous envoyer ce repository.

Le test doit contenir les tests unitaires et d'intégrations associés aux développements faits